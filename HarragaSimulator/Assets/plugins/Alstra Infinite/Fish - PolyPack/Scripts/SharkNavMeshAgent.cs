using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class SharkNavMeshAgent : MonoBehaviour
{
    private GameObject target;
    public Transform targetTransform;
    Vector3 destination;
    NavMeshAgent agent;

    void Start()
    {
        // Cache agent component and destination
        target = GameObject.FindWithTag("Boat");
        if (target != null) targetTransform = target.transform;
        else Debug.LogError("Couldn't find the Boat to target!");
        agent = GetComponent<NavMeshAgent>();
        destination = agent.destination;
    }

    void Update()
    {
        // Update destination if the target moves one unit
        if (Vector3.Distance(destination, targetTransform.position) > 1.0f)
        {
            destination = targetTransform.position;
            agent.destination = destination;
        }
    }
}