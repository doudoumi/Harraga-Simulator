using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject MainMenuHolder;
    public GameObject OptionsHolder;


    public void PlayGame() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private bool isOptions = false;
    public void ToggleOptions() 
    {
        // yes thats how i toggle stuff, whatchout big brain code:
        MainMenuHolder.SetActive(isOptions);
        OptionsHolder.SetActive(!isOptions);

        isOptions = !isOptions;
    }
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
