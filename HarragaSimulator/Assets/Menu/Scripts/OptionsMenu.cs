using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;

public class OptionsMenu : MonoBehaviour
{
    public Dropdown QualityDropDown;

    UniversalRenderPipelineAsset urpAsset;
    void Start() {
        urpAsset = (UniversalRenderPipelineAsset)GraphicsSettings.renderPipelineAsset;

        // TODO: improve this shit
        switch (urpAsset.renderScale) {
            case 0.5f:
                QualityDropDown.value = 0;
                break;
            case 0.75f:
                QualityDropDown.value = 1;
                break;
            case 1.0f:
                QualityDropDown.value = 2;
                break;
            default:
                QualityDropDown.value = 1;
                break;
        }
    }

    public void SetQuality() {
        switch (QualityDropDown.value) {
            case 0:
                urpAsset.renderScale = 0.5f;
                break;
            case 1:
                urpAsset.renderScale = 0.75f;
                break;
            case 2:
                urpAsset.renderScale = 1.0f;
                break;
            default:
                urpAsset.renderScale = 0.75f;
                break;
        }
    }
}
