using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    /*
    public void OpenPauseMenu() 
    {
        pauseMenu = GetComponent<Canvas>();
        Time.timeScale = 0;
        pauseMenu.gameObject.SetActive(true);
    }
    */
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}