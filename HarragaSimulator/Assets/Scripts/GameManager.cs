using UnityEngine;

public class GameManager : MonoBehaviour
{
    public OceanGenerator ocean;
    public ImmigrantPort immigrantPort;
    public GameObject mainBoat_prefab;
    public GameObject shark_prefab;
    public GameObject floatingStuff_prefab;
    public GameObject ui_prefab;

    // Start is called before the first frame update
    void Start()
    {
        // limit fps
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;


        // init ocean
        ocean.init();

        // init boat
        GameObject boat = Instantiate(mainBoat_prefab);

        // init UI
        GameObject gameUI = Instantiate(ui_prefab);

        // init immigrant port
        immigrantPort.init();

        // spawn some crates
        GameObject cratesHolder = new GameObject("Crate Holder");
        for (int i = 0; i < 5; i++) {
            GameObject crate = Instantiate(floatingStuff_prefab, new Vector3(Random.Range(-20.0f, -100.0f), 1.0f, Random.Range(-200.0f, 200.0f)), Quaternion.identity);
            crate.transform.SetParent(cratesHolder.transform);
        };

        // shark
        Instantiate(shark_prefab, new Vector3(0, -5, -250), Quaternion.identity);
    }
}
