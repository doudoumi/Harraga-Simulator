using UnityEngine;
using TMPro;

public class FPSCounter : MonoBehaviour
{
    private TextMeshProUGUI fpsText;
    public float fpsRefreshRate = 0.1f;

    private void Start() {
        fpsText = GetComponent<TextMeshProUGUI>();
    }

    private float timer = 0.0f;
    void Update() {
        if (timer > fpsRefreshRate) {
            fpsText.text = (int)(1f / Time.deltaTime) + " (" + Mathf.Round(Time.unscaledDeltaTime * 1000) + " ms)";
            timer = 0.0f;
        }
        timer += Time.deltaTime;
    }
}
