using UnityEngine;
using TMPro;

public class MoneyCalculation : MonoBehaviour
{ 
    private TextMeshProUGUI moneyText;
    private void Start()
    {
        moneyText = GetComponent<TextMeshProUGUI>();
    }
    void Update()
    {
        moneyText.text = ImmigrantPortArrive.getMoney() + " DA";
    }
}
