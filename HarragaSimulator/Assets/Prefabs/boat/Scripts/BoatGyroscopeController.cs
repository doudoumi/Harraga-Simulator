using UnityEngine;

public static class DeviceRotation {
    private static bool gyroInitialized = false;

    public static bool HasGyroscope {
        get {
            return SystemInfo.supportsGyroscope;
        }
    }

    public static Quaternion Get() {
        if (!gyroInitialized) {
            InitGyro();
        }

        return HasGyroscope
            ? ReadGyroscopeRotation()
            : Quaternion.identity;
    }

    private static void InitGyro() {
        if (HasGyroscope) {
            Input.gyro.enabled = true;                // enable the gyroscope
            Input.gyro.updateInterval = 0.00694f;    // set the update interval to 144 hz
        }
        gyroInitialized = true;
    }

    private static Quaternion ReadGyroscopeRotation() {
        return new Quaternion(0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude * new Quaternion(0, 0, 1, 0);
    }
}

public class BoatGyroscopeController : MonoBehaviour {
    /*
#if UNITY_ANDROID && !UNITY_EDITOR
    void Update() {

        Quaternion mRotation = DeviceRotation.Get();
        Quaternion mLock = mRotation;

        // Lock x axis still need to test to adapt ramzi
        if (mRotation.eulerAngles.x > 20 && mRotation.eulerAngles.x <= 180) {
            mLock = Quaternion.Euler(20, mRotation.eulerAngles.y, mRotation.eulerAngles.z);
        }
        else if (mRotation.eulerAngles.x < 340 && mRotation.eulerAngles.x > 180) {
            mLock = Quaternion.Euler(340, mRotation.eulerAngles.y, mRotation.eulerAngles.z);
        }

        // Lock z axis still need to test to adapt ramzi 
        if (mRotation.eulerAngles.z > 20 && mRotation.eulerAngles.z <= 180) {
            mLock = Quaternion.Euler(mRotation.eulerAngles.x, mRotation.eulerAngles.y,20);
        }
        else if (mRotation.eulerAngles.z < 340 && mRotation.eulerAngles.z > 180) {
            mLock = Quaternion.Euler(mRotation.eulerAngles.x, mRotation.eulerAngles.y, 340);
        }

        GetComponent<Rigidbody>().transform.rotation = mLock;
    }
#endif
    */
}
