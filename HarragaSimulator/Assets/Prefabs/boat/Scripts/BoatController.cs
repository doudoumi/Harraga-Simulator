using UnityEngine;
using StylizedWater;
using UnityStandardAssets.CrossPlatformInput;

public class BoatController : MonoBehaviour
{
    private Rigidbody rigidBody;
    private Transform ocean;
    public Transform rudder;

    public float thrust = 1500.0f;
    public float maxSpeed = 10.0f;
    public float rotateSpeed = 5.0f;

    // Wave properties
    private float steepness;
    private float wavelength;
    private float speed;
    private float[] directions = new float[4];
    private StylizedWater.StylizedWaterURP water;
    public Transform motorPosition;

    private float wavesHeightByDistance = 2000.0f;

    private void Start()
    {
        water = GameObject.FindGameObjectsWithTag("Ocean")[0].GetComponent<StylizedWater.StylizedWaterURP>();
        //water.SetWaveSteepness(2.0f);

        rigidBody = GetComponent<Rigidbody>();
        ocean = GameObject.FindGameObjectsWithTag("Ocean")[0].transform;
    }

    void FixedUpdate()
    {
        // Get wave properties from water
        steepness = water.GetWaveSteepness();
        wavelength = water.GetWaveLength();
        speed = water.GetWaveSpeed();
        directions = water.GetWaveDirections();

        float waveHeight = water.transform.position.y + GerstnerWaveDisplacement.GetWaveDisplacement(motorPosition.position, steepness, wavelength, speed, directions).y;

        if (motorPosition.position.y < waveHeight) // only if motor under water
            if (CrossPlatformInputManager.GetAxis("Vertical") != 0) {
                rigidBody.AddForce(transform.forward * CrossPlatformInputManager.GetAxis("Vertical") * thrust * Time.fixedDeltaTime);

//#if !UNITY_ANDROID || UNITY_EDITOR
                transform.Rotate(
                    new Vector3(
                        0,
                        Mathf.Sign(CrossPlatformInputManager.GetAxis("Vertical")) * // multiply by movement direction
                            CrossPlatformInputManager.GetAxis("Horizontal"), 
                        0
                    ) *
                    rigidBody.velocity.magnitude * // if no speed no rotation (its not a tank)
                    rotateSpeed * 
                    Time.fixedDeltaTime
                , Space.World);
//#endif
            };

        // rudder turn
        rudder.localRotation = Quaternion.Euler(-90, -CrossPlatformInputManager.GetAxis("Horizontal") * 45, 0);

        rigidBody.velocity = Vector3.ClampMagnitude(rigidBody.velocity, maxSpeed);

        // move ocean plane
        ocean.position = new Vector3(transform.position.x, 0.0f, transform.position.z);

        // set waves heights
        water.SetWaveSteepness(Mathf.Abs(transform.position.z / wavesHeightByDistance));
        //water.GetWaveLength(12.0f);
        water.UpdateWaves();
    }
};