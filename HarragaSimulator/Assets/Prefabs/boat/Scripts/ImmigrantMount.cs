using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmigrantMount : MonoBehaviour
{
    // called when shere collider is triggered
    private void OnTriggerEnter(Collider coll)
    {
        // if immigrant body detected add it to food list
        if(coll.gameObject.tag == "ImmigrantBody") {
            coll.gameObject.transform.position = transform.position; 
        } else if(coll.gameObject.tag == "Crate") { // also Crates
            coll.gameObject.transform.position = transform.position; 
        };
    }
    
}
