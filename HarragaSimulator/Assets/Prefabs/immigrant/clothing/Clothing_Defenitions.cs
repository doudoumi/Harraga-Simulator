using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Clothing_Defenitions
{
    public static Color[] colors = new Color[]{
        new Color(1.0f, 0.75f, 0.0f), // nice yellow
        new Color(0.93f, 0.09f, 0.12f), // nice red
        new Color(0.75f, 0.0f, 0.37f), // nice red/pink
        new Color(0.42f, 0.15f, 0.55f), // nice purple
        new Color(0.0f, 0.2f, 0.73f), // nice blue
        new Color(0.0f, 0.53f, 0.75f), // nice light blue
        new Color(0.0f, 0.48f, 0.75f), // nice green
        new Color(0.41f, 0.71f, 0.0f), // nice light green
        new Color(0.0f, 0.56f, 0.13f), // another nice green
        Color.black,
        Color.white,
    };
}
