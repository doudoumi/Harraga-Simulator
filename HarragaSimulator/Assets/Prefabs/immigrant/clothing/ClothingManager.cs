using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothingManager : MonoBehaviour
{
    public SkinnedMeshRenderer immigrantMesh;
    public Transform mainBone;

    public List<GameObject> equipments;

    void Start() {
        GameObject ClothingHolder = new GameObject("Clothing"); // just for the inspector
        ClothingHolder.transform.SetParent(transform);

        for (int i = 0; i < equipments.Count; i++) {
            // create the clothing
            GameObject equipment_temp = Instantiate(equipments[i]);
            equipment_temp.transform.SetParent(ClothingHolder.transform);
            
            // attach to immigrant rig
            for (int j = 0; j < equipment_temp.transform.childCount; j++) {
                Equipmentizer equipmentizer = equipment_temp.transform.GetChild(j).GetComponent<Equipmentizer>();
                if (equipmentizer == null) continue;

                equipmentizer.TargetMeshRenderer = immigrantMesh;

                SkinnedMeshRenderer meshRendrer = equipment_temp.transform.GetChild(j).GetComponent<SkinnedMeshRenderer>();
                meshRendrer.rootBone = mainBone; // set immigrant root bone
                meshRendrer.materials[0].color = Clothing_Defenitions.colors[Random.Range(0, Clothing_Defenitions.colors.Length)];
            };
        };
    }


}
