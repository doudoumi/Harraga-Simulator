using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinColor : MonoBehaviour
{
    public List<Material> skins;

    void Start()
    {
        SkinnedMeshRenderer immigrantMesh = GetComponent<SkinnedMeshRenderer>();

        // set random skin material
        immigrantMesh.material = skins[Random.Range(0, skins.Count)];
    }
}
