using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class StickImmigrantToBoat : MonoBehaviour
{
    private float attachBreakForce = 3.5f;
    private Rigidbody boat_rb;

    void Start()
    {
        boat_rb = GameObject.FindGameObjectsWithTag("Boat")[0].GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider coll)
    {

        if(coll.gameObject.tag == "ImmigrantAttach") {
            // return if is already attached
            if (gameObject.GetComponent<FixedJoint>() != null) return;

            FixedJoint fixedJoints = gameObject.AddComponent(typeof(FixedJoint)) as FixedJoint;
            fixedJoints.connectedBody = boat_rb;
            fixedJoints.breakForce = attachBreakForce;
        };
    }
}
