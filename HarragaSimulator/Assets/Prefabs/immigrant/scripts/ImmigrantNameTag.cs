using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImmigrantNameTag : MonoBehaviour
{
    private string[] potentialName = new string[] {
        "Zhey",
        "Zahro",
        "Zhahreddine",
        "Sid Ali",
        "Ramzi",
        "Adem",
        "Nadyl",
        "3imad",
        "3athman",
        "Morad",
        "Moh",
        "Raouf",
        "Bou3lam",
        "Karim",
        "AbdelCrime",
        "Lotfi",
        "Yassine"
    };

    private string[] potentialNickName = new string[] {
        "el gat",
        "el ghoule"
    };

    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = GetRandomName();
    }
    private string GetRandomName() 
    { 
        int index = Random.Range(0, potentialName.Length - 1);
        return potentialName[index];
    }
}
