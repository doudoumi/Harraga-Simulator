using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkMouth : MonoBehaviour
{
    public Shark shark;
    public ParticleSystem blood_ps;

    private GameObject bittenObject;
    private float timeToDie = 10.0f;
    private float bitTime = 0.0f;

    public void FixedUpdate() {
        if (bittenObject != null) {
            bittenObject.transform.position = transform.position;
            bitTime += Time.fixedDeltaTime;

            // play blood particles
            blood_ps.Play();

            // food is dead
            if (bitTime > timeToDie) {
                Destroy(bittenObject.transform.parent.transform.parent.gameObject); // temp, do it better later
                bittenObject = null;
            };
        } else {
            // stop blood particles in not bittin anything
            blood_ps.Stop();
        }
    }

    // called when shere collider is triggered
    private void OnTriggerEnter(Collider coll)
    {
        // if immigrant body detected add it to food list
        if(coll.gameObject.tag == "ImmigrantBody") {
            bittenObject = coll.gameObject;
            bitTime = 0.0f;
        };
    }
    
    private void OnTriggerExit(Collider coll)
    {
        // for later
    }
}
