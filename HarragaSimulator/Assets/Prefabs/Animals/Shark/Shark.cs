using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StylizedWater;

public class Shark : MonoBehaviour
{
    [HideInInspector]
    public GameObject boat;
    private Rigidbody rb;
    public List<GameObject> food;
    private StylizedWater.StylizedWaterURP water;

    public GameObject target;
    private float movement_spd = 1000.0f; // 1/3 rotation_spd to prevent it from orbitting the target
    private float rotation_spd = 3.0f;

    public float difficulty = 2.0f; 

    private float foodTooFarFromBoat = 50.0f;

    // Wave properties
    private float steepness;
    private float wavelength;
    private float speed;
    private float[] directions = new float[4];

    void Start()
    {
        water = GameObject.FindGameObjectsWithTag("Ocean")[0].GetComponent<StylizedWater.StylizedWaterURP>();
        boat = GameObject.FindGameObjectsWithTag("Boat")[0];

        rb = GetComponent<Rigidbody>();
        List<GameObject> food = new List<GameObject>();
    }

    void FixedUpdate()
    {
        // apply gravity if over water
        steepness = water.GetWaveSteepness();
        wavelength = water.GetWaveLength();
        speed = water.GetWaveSpeed();
        directions = water.GetWaveDirections();

        float waveHeight = water.transform.position.y + GerstnerWaveDisplacement.GetWaveDisplacement(transform.position, steepness, wavelength, speed, directions).y;

        if (transform.position.y > waveHeight) rb.useGravity = true;
        else rb.useGravity = false;

        // if food list contains something, attack it.
        target = getClosestFood();
        if (target == null) target = boat;

        // rotate to food
        Vector3 posToTarget = target.transform.position - transform.position;
        var rotation = Quaternion.LookRotation(posToTarget, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotation_spd * difficulty * Time.fixedDeltaTime);

        // move to target
        rb.AddForce(
            transform.forward * 
            Mathf.Clamp(Vector3.Dot(transform.forward, posToTarget), 0.0f, 1.0f) * 
            movement_spd * difficulty * Time.fixedDeltaTime
        );
    }

    private GameObject getClosestFood() {
        // remove dad immigrants
        // must be backwards to remove object from list while iterating !
        float closestFoodDistance = float.MaxValue;
        GameObject closestFood = null;
        for (int i = food.Count-1; i >= 0; i--) {
            if (food[i] == null) {
                food.RemoveAt(i);
            } else if (Vector3.Distance(food[i].transform.position, boat.transform.position) >= foodTooFarFromBoat) {
                food.RemoveAt(i);
            } else {
                float distanceFood = Vector3.Distance(food[i].transform.position, transform.position);
                if (distanceFood < closestFoodDistance) {
                    closestFood = food[i];
                    closestFoodDistance = distanceFood;
                };
            };
        };

        // can return null value
        return closestFood;
    }
}
