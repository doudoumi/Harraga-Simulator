using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkSensor : MonoBehaviour
{
    public Shark shark;

    // called when shere collider is triggered
    private void OnTriggerEnter(Collider coll)
    {
        // if immigrant body detected add it to food list
        if(coll.gameObject.tag == "ImmigrantBody") {
            // don't add if already in it
            if (!shark.food.Contains(coll.gameObject)) {
                shark.food.Add(coll.gameObject);
            };
        };
    }
    
    private void OnTriggerExit(Collider coll)
    {
        // for later
        
    }
}
