using UnityEngine;

public class ImmigrantPortArrive : MonoBehaviour
{
    private static int money = 0;
    // called when sphere collider is triggered
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "ImmigrantBody")
        {
            Destroy(coll.gameObject.transform.parent.gameObject.transform.parent.gameObject);
            //coll.gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.position = GetComponent<BoxCollider>().transform.position + new Vector3(0.0f, 10.0f, 0.0f);// Pour le mettre sur un transate plus tard
            money += 50;
        };

    }

    public static int getMoney() 
    {
        return money;
    }
}
