using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmigrantPort : MonoBehaviour
{
    public GameObject player_prefab;
    private GameObject immigrantsHolder;

    private Vector2 respawnTime = new Vector2(0.0f, 1.0f);

    public List<Transform> immigrantSpawnPoints;
    public List<GameObject> waitingImmigrants;

    // Start is called before the first frame update
    void Start()
    {
        immigrantsHolder = new GameObject("Immigrant Holder");
    }

    public void init() {
        for (int i = 0; i < immigrantSpawnPoints.Count; i++) {
            spawnImmigrant(immigrantSpawnPoints[i].position);
        };
    }

    // called when sphere collider is triggered
    private void OnTriggerEnter(Collider coll)
    {
        // if immigrant body detected add it to food list
        if(coll.gameObject.tag == "Boat") {
            if (waitingImmigrants.Count == 0) return;

            for (int i = waitingImmigrants.Count-1; i >= 0; i--) {
                if (waitingImmigrants[i] == null) continue;
                else {
                    waitingImmigrants[i].transform.position = coll.gameObject.transform.position + new Vector3(0.0f, 3.0f, 0.0f);
                    waitingImmigrants.RemoveAt(i);
                    break;
                };
            };


            StartCoroutine(spawnImmigrants(Random.Range(respawnTime.x, respawnTime.y), immigrantSpawnPoints[0].position));
        };
    }

    IEnumerator spawnImmigrants(float delay, Vector3 position) {
        yield return new WaitForSeconds(delay);
        spawnImmigrant(position);
    }

    private void spawnImmigrant(Vector3 position) {
        GameObject immigrant_tmp = Instantiate(player_prefab, position, Quaternion.identity);
        immigrant_tmp.transform.SetParent(immigrantsHolder.transform);
        immigrant_tmp.name = "Immigrant";
        waitingImmigrants.Add(immigrant_tmp);
    }
}
