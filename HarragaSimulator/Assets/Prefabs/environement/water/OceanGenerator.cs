using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanGenerator : MonoBehaviour
{
    public GameObject oceanChunk_prefab;

    private int nbrOceanChunks = 4;
    private float oceanChunkSize = 7.5f;

    private GameObject ocean_main;

    public void init() {
        ocean_main = GameObject.Instantiate(oceanChunk_prefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
        ocean_main.transform.SetParent(gameObject.transform);
        ocean_main.tag = "Ocean";

        ocean_main.transform.localScale = new Vector3(20.0f, 1.0f, 20.0f);
/*
        // generate ocean chunks
        for(int i = 0; i < nbrOceanChunks; i++) {
            crteateSquareChunks(i);
        };
*/
    }

    private void crteateChunk(Vector2 position, Vector2 scale) {
        GameObject ocean_tmp = GameObject.Instantiate(oceanChunk_prefab, new Vector3(position.x, 0, position.y), Quaternion.identity);
        ocean_tmp.transform.localScale = new Vector3(scale.x, 1.0f, scale.y); // set scale

        // for heiharchy
        ocean_tmp.transform.SetParent(ocean_main.transform);
        ocean_tmp.name = "Ocean Chunk";
    }

    private void crteateSquareChunks(int squareOctave) {
        float size = Mathf.Pow(3.0f, squareOctave);

        // create a square (do it in a loop later)
        crteateChunk(new Vector2(oceanChunkSize *  size, oceanChunkSize *  size), new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize *  0,    oceanChunkSize *  size), new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize *  size, oceanChunkSize *  0),    new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize * -size, oceanChunkSize * -size), new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize *  0,    oceanChunkSize * -size), new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize * -size, oceanChunkSize *  0),    new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize *  size, oceanChunkSize * -size), new Vector2(size, size));
        crteateChunk(new Vector2(oceanChunkSize * -size, oceanChunkSize *  size), new Vector2(size, size));
    }

}
