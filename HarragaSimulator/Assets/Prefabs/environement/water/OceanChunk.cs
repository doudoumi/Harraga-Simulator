using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanChunk : MonoBehaviour
{
    void Start()
    {
        // reset mesh bonds for camera cuilling
        Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
        mesh.bounds = new Bounds(new Vector3(0.0f, 1.0f, 0.0f), new Vector3(8.0f, 4.0f, 8.0f));
    }
}
